/* Fill-in information from Blynk Device Info here */
#define BLYNK_TEMPLATE_ID "TMPL6-zZr3Gsk"
#define BLYNK_TEMPLATE_NAME "Raabbit"
#define BLYNK_AUTH_TOKEN "I5TM6zPVj_7D6SGJVtr7RpWEmJ7NY5xZ"

/* Comment this out to disable prints and save space */
#define BLYNK_PRINT Serial


#include <WiFi.h>
#include <WiFiClient.h>
#include <BlynkSimpleEsp32.h>

#include <DHT.h>

// Your wifi information
const char* ssid = "Wokwi-GUEST";
const char* password = "";

#define DHTPIN 32
const int ledWifi = 4; //LED WIFI
const int ledBlynk = 2; //LED Blynk
const int Relay = 16;
int soilPin = 33;
unsigned long prv = 0;


int relayState = 0;
bool isBlynkConnected = false;
DHT dht(DHTPIN, DHT22);

void connectWifi() {
  WiFi.begin(ssid, password);
  Serial.print("WiFi: Connecting");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("\nWiFi connected.");
}

BlynkTimer timer;
BLYNK_WRITE(V2)     // Virtual pin V2 in the Blynk app controls the waterpump.
{  
  relayState = param.asInt();

  if (relayState == 1) 
    digitalWrite(Relay, HIGH);

  else
    digitalWrite(Relay, LOW);  
}

void sendSensorData() {
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  int soil = analogRead(soilPin);
  int moisture = map(soil, 0, 1023, 100, 0);

  // Check if Blynk is connected before sending data
  if (Blynk.connected()) {
    Blynk.virtualWrite(V6, t); // Update temperature value on Blynk app
    Blynk.virtualWrite(V5, h); // Update humidity value on Blynk app
    Blynk.virtualWrite(V1, moisture); // Update moisture value on Blynk app
  }

  //check 
  if(moisture >= 30){
    digitalWrite(Relay, HIGH);
  }
  if(t >= 28){
    digitalWrite(Relay, HIGH);
  }

  // Print sensor readings
  if (millis() - prv >= 5000) { 
    Serial.print("Temp: "); 
    Serial.print(t); 
    Serial.println(" *C"); 
    Serial.print("Humidity: "); 
    Serial.print(h); 
    Serial.println("%"); 
    Serial.print("Moisture: ");
    Serial.print(moisture); 
    Serial.println("%"); 
    prv = millis();
  } 
}

void setup() {
  Serial.begin(9600);
  Blynk.begin(BLYNK_AUTH_TOKEN, ssid, password);
  connectWifi();
  dht.begin();

  pinMode(ledWifi, OUTPUT);
  pinMode(ledBlynk, OUTPUT);
  pinMode(Relay, OUTPUT);
  digitalWrite(Relay, LOW);

  timer.setInterval(5000L, sendSensorData); // Set the interval for sending sensor data
}

void loop() {
  // Check WiFi connection status and update LED
  if (WiFi.status() == WL_CONNECTED) {
    digitalWrite(ledWifi, HIGH); // Turn on LED when connected
  } else {
    digitalWrite(ledWifi, LOW); // Turn off LED when disconnected
  }

  isBlynkConnected = Blynk.connected();
  
  if (isBlynkConnected) {
    digitalWrite(ledBlynk, HIGH); // Turn on the LED when Blynk is connected
  } else {
    digitalWrite(ledBlynk, LOW); // Turn off the LED when Blynk is disconnected
  }

  Blynk.run();
  timer.run();
  digitalWrite(ledWifi, HIGH);
}
